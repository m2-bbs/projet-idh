# Projet IDH - Partie individuelle : *Pseudomonas putida*

*Jérémy MARTIN - Université Paul Sabatier, Toulouse - Master Bioinformatique 2019-2021*

[[_TOC_]]

# Introduction

Dans le cadre de l'UE Intégration de Données Hétérogène du Master de Bioinformatique de Toulouse, il a été demandé de compléter une base de données de type graphe initiée en cours.

Lors de l'atelier dédié, nous avions au préalable créé une base de données contenant les annotations de la GeneOntology du micro-organismes *Escherichia coli*. Ce projet consiste à récupérer les informations d'un autre organisme contenu dans **STRINGdb**, les implémenter dans la base de données **Neo4J** réalisé en TP et étudier le niveau d'annotation de l'organisme choisi.

L'organisme qui va nous intéresser est *Pseudomonas putida*. Il s'agit d'une bactérie saprophyte des sols, qui joue un rôle dans certains procédés industriels de fertilisations. Bien que ne faisant pas partie des bactéries strictement pathogènes, elle a été reconnu pathogène opportuniste chez les patients à terrain immunodéprimé dans les années 80[^1.] et présente un risque sanitaire dans le cadre d'infections nosocomiales[^2.]

[^1.]: Anaissie E, Fainstein V, Miller P, Kassamali H, Pitlik S, Bodey GP, et al. *Pseudomonas putida. The American Journal of Medicine. 1987;82:1191‑4.*
[^2.]:Yoshino Y, Kitazawa T, Kamimura M, Tatsuno K, Ota Y, Yotsuyanagi H. *Pseudomonas putida bacteremia in adult patients: five case reports and a review of the literature. J Infect Chemother. 2011;17:278‑82.*

# Prise en main des données

L'intégralité des scripts Rmd, les compilations html, et les fichiers de données sont disponible dans le dépôt gitlab du projet au lien suivant : [gitlab.com/m2-bbs/projet-idh.git](https://gitlab.com/m2-bbs/projet-idh.git).

Les données utilisés pour l'étape d'intégration de données sont les données issus de :

* **Reconstruction de la base de donées à l'aide de** [**STRINGdb v11**](https://version-11-0.string-db.org/cgi/download.pl?sessionId=ktSgjfcvDSWp&species_text=Pseudomonas+putida+KT2440)
	* [160488.proteins.links.v11.0.txt.gz](https://stringdb-static.org/download/protein.links.v11.0/160488.protein.links.v11.0.txt.gz)
	* [160488.proteins.links.detailed.v11.0.txt.gz](https://stringdb-static.org/download/protein.links.detailed.v11.0/160488.protein.links.detailed.v11.0.txt.gz)
	* [160488.protein.actions.v11.0.txt.gz](https://stringdb-static.org/download/protein.actions.v11.0/160488.protein.actions.v11.0.txt.gz)
* **Reconstruction de l'annotation à l'aide de l'**[**EBI : Annotations des protéomes par la GeneOntology**](ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/)
	* [109.P_putida_KT2440.goa](ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/109.P_putida_KT2440.goa)
* **Recherche des orthologues à partir de** [**STRINGdb v11:**](https://version-11-0.string-db.org/cgi/download.pl)
	* [COG.mappings.v11.txt.gz](https://stringdb-static.org/download/COG.mappings.v11.0.txt.gz)

La méthode d'intégration dans Neo4j est entièrement détailler dans le fichier [`doc_rmd/P_putida_databases.Rmd`](https://gitlab.com/m2-bbs/projet-idh/-/blob/master/doc_rmd/P_putida_databases.Rmd).

## GO Annotations

Concernant le niveau d'annotation de *Pseudomonas putida*, on retrouve dans STRINGdb 5,350 références..

Au total, ce sont 27,396 annotations présents dans la GeneOntology sur 4,039 protéines différentes (soit 75.5% des protéines de STRINGdb). Ces annotations nous informent qu'une grande partie des protéines jouent un rôle dans les fonctions moléculaire du micro-organisme, mais aussi que plus d'un quart d'entre elles sont annotées comme appartenant aux trois ontologies (*fig1 et 2*).

![Fig1: Barplot de la GO annotation](fig/go_barplot.png)

_**Fig1:** Barplot de la GO annotation_

<img src="fig/go_venn.png" width="351" height="284">

_**Fig2:** Diagramme de Venn de la GO annotation_

## Rechercher d'orthologues

Ici aussi, la procédure pour accéder aux orthologues est présenté dans le fichier [`doc_rmd/P_putida_databases.Rmd`](https://gitlab.com/m2-bbs/projet-idh/-/blob/master/doc_rmd/P_putida_databases.Rmd#L484)

Il a été caractérisés 20,475 liens d'orthologie entre *Pseudomonas putida* et *Escherichia coli*. Ces liens sont répartis entre 3,804 protéines de *P. putida* et 2,980 protéines de *E. coli*.

Pour rappel, il y a chez *P. putida* 5,350 protéines et chez *E. coli* 4,127 protéines.

Cela signifirait que 71% des protéines **annotées** de *P. putida* serait orthologues à 72% des protéines d'*E. coli*. 

## De: blastset.py À: blastset_v2.py

Il nous a été fournis le script `blastset.py` depuis le [gitlab de Roland Barriot](https://gitlab.com/rbarriot/enrichment.git), et il nous a été demandé de le compléter en ajoutant les méthodes Chi2 et Coverage.

L'utilisation de ces deux méthodes se fait de la même manières qu'avec les méthodes précédentes :

```python
usage: blastset_v2.py [-h] -q QUERY -t SETS [-a ALPHA] [-c] [-m MEASURE] [-l LIMIT]

Search enriched terms/categories in the provided (gene) set

optional arguments:
  -h, --help            		show this help message and exit
  -q QUERY, --query QUERY		Query set.
  -t SETS, --sets SETS  		Target sets (categories).
  -a ALPHA, --alpha ALPHA		Significance threshold.
  -c, --adjust					Adjust for multiple testing (FDR).
  -m MEASURE, --measure MEASURE	Dissimilarity index: 
								binomial (default), hypergeometric, chi2 or coverage.
  -l LIMIT, --limit LIMIT		Maximum number of results to report.
```

### Ajout de Chi2

Le package `scipy` étant déjà importé, nous avons pris le choix d'utiliser la fonction `chi2_contingency`.  Pour l'utiliser il a fallut calculer la table de contingence à donner en paramètre de cette fonction.

Cette table a été calculé selon les consignes du projet :

|           | **T**     | **G / T**         | $`\sum`$ |
| --------- | :---: | :-----------: | :----: |
| **Q**     | c     | q - c         | q      |
| **G / Q** | t - c | g - q - t + c | g - q  |
| $`\sum`$    | t     | g - t         | g      |

En considérant un ensemble de requête Q (*query*) et un ensemble cible T (*target*). Les deux ensembles sont inclus dans l'ensemble des gènes de l'organisme G (*génome*), q, t et g sont leurs cardinalités respectives.

Une fois la table créée, la fonction `chi2_contingency` retourne la p-valeur (mais pas que) du test réalisé.

### Ajout de Coverage/Overlap

Pour ce qui est du coverage (ou overlap), on cherche a savoir quelles fractions des deux ensembles se correspondent. Ici aussi, nous nous sommes basé sur les consignes du projet :

$`coverage=1 - (\frac{c}{q} * \frac{c}{t})`$ 

Il est important de noter que le test de coverage ne retourne pas a proprement parlé une p-value, mais un indice de "dissemblance" qui varie entre 0 et 1. Si le coverage est égal à 0, la requête et la cible sont identiques, si il est égal à 1, alors il n'y a aucune ressemblance.

# Gene Set Enrichment Analyzes

## Analyse

L'analyse d'enrichissement est détaillée dans le fichier [`doc_rmd/Analyse_P_putida.Rmd` (l.484)](https://gitlab.com/m2-bbs/projet-idh/-/blob/master/doc_rmd/Analyse_P_putida.Rmd).

## Résultats

Ci dessous un tableau récapitulatif des résultats retournés par nos analyses :

|                    | Nombre de matchs | P-value min | P-value max |
| ------------------ | :--------------: | :---------: | :---------: |
| **Binomial**       |       398        |  2.996e-87  |  0.002154   |
| **Hypergeometric** |       411        | 4.813e-105  |  0.002028   |
| **Chi2**           |       771        |      0      |  0.002678   |
| **Coverage**       |       1925       |   0.2892    |   0.99997   |

Dans notre cas, il semblerait que la méthode 'hypergeometric' ou 'chi2' soient de bon compromis entre qualités des p-values et nombre de matchs réalisé.

En regardant de plus près les termes GO retourné, on se rend compte que :

- tout les termes GO de la méthode `binomial` sont présent dans les résultats de la méthode `hypergeometric`
- tous les termes GO de la méthode `hypergeometric` sont présent dans les résultats de la méthode `chi2`
- tous les termes GO de la méthode `chi2` sont présent dans les résultats de la méthode `coverage`

```R
# Binomial / Hypergeometrique
 all(binom.data[,1] %in% hyper.data[,1])
[1] TRUE

# Hypergeometric / Chi2
 all(hyper.data[,1] %in% chi2.data[,1])
[1] TRUE

# Chi2 / Coverage
 all(chi2.data[,1] %in% cov.data[,1])
[1] TRUE
```

La méthode `chi2` semble donc la plus adaptée à nos données. En effet, les résultats obtenus par cette méthode contient toutes les GO Annotation retournées par les méthodes `binomial` et `hypergeometric`, et tous les matchs retournés par ces trois méthodes retournent un p-value inférieur à 0.05 et sont donc significatifs.

## Le cas coverage

Comme expliqué plus haut, le coverage n'est pas définie par une p-value mais par un "indice de dissimilarité". Une étude des résultats de cette méthode montre que les ensembles ont tendances a être différents.

ll a été mis en évidence un peu plus haut, le fait que cette dissimilarité semblait être hétérogènes (min à 0.29 et max à +/- 1).

Une étude un peu plus poussé du fichier [`enrichment.putida/pputida.coverage.enriched.tsv`](https://gitlab.com/m2-bbs/projet-idh/-/blob/master/enrichment.putida/pputida.coverage.enriched.tsv) a permis d'établir que la tendance est en faveur d'une forte dissimilarité (*fig3*). Il n'y a dans les faits que 35 matchs présentant un indice inférieur à 0.80, et 5 matchs présentant un indice inférieur à 0.5 (soit respectivement env. 2 et 0.3% du nombre total de matchs)

![Fig3: Histogramme de l'indice de dissimilarité de coverage](fig/cov_hist.png)

_**Fig3:** Histogramme de l'indice de dissimilarité de coverage_

# Bilan

L'intégration de données hétérogènes est un exercice de plus en plus mis en avant pour pouvoir traiter de grosses quantitées de données, mais aussi pour pouvoir comparer deux expériences qui ne sembleraient pas initialement comparables.

Cette discipline grandissante dans les domaines de la recherche scientifique et devenue quasiement incontournable dans les exercices de traitements de données -omics.

En dehors des difficultés induites par Neo4j et la prise en main chaotiques des bases de données de type graphe, j'ai trouvé ce projet intéressant et il m'a aidé à ouvrir les yeux sur le fait que ce n'est pas car une méthode retourne le plus d'informations qu'elle est forcément pertinente (cf. la méthode coverage qui retourne près de 2,000 matchs mais qui n'est finalement que peu informative).

Ce projet a également été l'occasion de se replonger dans l'utilisation de R, exercice que je n'avais pas eu l'occasion de pratiquer pendant mon stage de Master 1 mais qui me sera d'une aide certaines pour mon stage de Master 2.

Parmi les points à améliorer, il y a notamment l'étude de l'orthologie entre les deux espèces. En effet, plus de 70% de protéines orthologues entre deux espèces de genre différents semblent peu probable, biologiquement parlant. Peut etre serait-il intéressant de filtrer les informations apportés par le fichier COG avec un seuil de score d'orthologie contenue dans les fichiers COG.links.v11.0.txt.gz ou COG.links.detailed.v11.0.txt.gz accessible depuis la page de [téléchargement de STRING-db](https://version-11-0.string-db.org/cgi/download.pl).
